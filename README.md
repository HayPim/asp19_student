# Laboratoire 2
## Introduction
Ce laboratoire permettra de comprendre et de mettre en oeuvre les techniques d’interfaçage des boutons poussoir et des leds par GPIO, les mécanismes de traitement des interruptions, la mise en oeuvre des timers et l’interfaçage de l’écran LCD. Il s'agira de configurer les différents registres du DM3730 nécessaires à l’initialisation des GPIO, des interruptions et des timers, et ensuite d'implémenter une Interrupt Service Routine (ISR) et les fonctions d’affichage LCD. [tiré du laboratoire]

Pour rendre fonctionnel les « includes », il faut ajouter les fichiers  init et toolbox au makefile initiale en générant les objet `.o`

## Etape 1 : Ecran LCD

L'écran à 2 dimensions, la hauteur et la largeur. Etant donné qu'il est représenté comme contiguë, nous pouvons l'adresser comme un tableau à 1 dimension. (X + Y * largeur écran) * 2. Le x2 est la conséquence que l'adresse d'un pixel est sur 16bits (pour gérer une multitude de couleur).

- `general_init` : Le but de cette fonction est d'initialiser la carte avec ses différents modules. (lcd, gpio, etc...)
- `clear_screen` : Fonction permettant d'effacer l'écran. Pour l'implémenter on parcours tous les pixels de l'écran et on lui attribue une couleur.
- `get_pixel_add` : Permet de recevoir la valeur le pixel 
- `fb_set_pixel` : Permet de mettre une valeur de pixel
- `fb_print_char` : Affiche un caractère. On utilise `fb_set_pixel` pour affecter chaque pixel que compose le tableau du charactère dans le fichier de fonts.
- `fb_print_string` : Affiche plusieurs caractère en les décalant chacun d'un espace d'un charactère.

Série de tests effectués : Toutes les fonctions on été testés dans le ??`main()` et fonctionne correctement.

## Etape 2 : Initialisation des GPIOs

Cette partie consiste à initialiser les GPIOs, pour ce faire, voici les étapes qui ont été effectués pour les configurer : 

- La première tâche à effectuer est de de faire un software reset.
- Ensuite, définir quels GPIOs sont des entées ou des sorties. LED0 et LED1 sont des output donc nous devons mettre le bit correspondant à 0. A savoir, BIT15 et BIT13. Tandis que les boutons sont des entrées donc on va  mettre leur bit à 1. (BIT12 SW1 et BIT14 SW2)
- Définir le `MXMode`, dans notre cas il faut le mettre à 4 pour les LEDs et pour les SWs dans le `PadConf` (définition de la configuragtion des GPIOs).
- Finalement, setter les registres de configuration avec les `PadConf` les leds et les boutons

Une fois que ces paramètres sont entrées, il est possible d'appeler la fonction `GPIO_init()` dans le `general_init()` du `main()` pour les initialiser au démarrage de la carte avec le code.

Afin de gérer le comportement des GPIO, nous avons créer un fichier toolbox avec toutes les fonction nécessaire à leur utilisation. (Qui a été ajouté par la suite au `Makefile`)
- `SetOutput` : Permet de définir l'état de sortie. On applique le masque permettant ainsi de renseigner l'état des GPIOs de la banque GPIO5 (et autres)
- `ClearOutput` : Permet de nettoyer l'état de sortie. On applique le masque qui va desactiver l'état des GPIOs de notre banque de GPIO5.
- `ReadInput` : Permet à l'aide du masque, de lire les données de notre GPIO dans la banque.
- `ToggleOutput` : Permet à l'aide du masque d'inverser le bit de notre GPIO en fonction de son masque et de sa banque.

Il y a 4 registres qui on été utilisés pour implémenter ces fonctions : 
`GPIO_DATAOUT` : Registre utilisé pour setter la valeur d'une GPIO définit comme sortante.  
`GPIO_DATAIN` : Registre utilisé pour lire la valeur d'une GPIO définit comme entante.
`GPIO_SETDATAOUT` : Registre permettant de mettre à 1 la valeur du bit correspondant à au registre de `GPIO_DATAOUT`
`GPIO_CLEARDATAOUT` : Clear à 0 le bit correspondant au bit dans le registre `GPIO_DATAOUT`.

## Etape 3 : Interruption par GPIOs

- La première tâche consiste à effectuer un software reset du MPU comme dans les précédentes sections.
- Il est nécessaire d'activer les interruptions du coté GPIO. Avant toute chose, il faut s'assurer qu'il est bien réinitialisé. Pour ce faire, nous initialisons le registre `GPIO_IRQSTATUS1` et définissons les interruption sur le franc montant grâce au registre `GPIO_RISINGDETECT`.
- Finalement, nous pouvons les activer en settant à 1 les interruptions depuis le boutons SW1 depuis le registre `GPIO_IRQENABLE1`
- Une fois la partie GPIO teminée, il faut s'occuper de la partie MPU. Le but est en premier lieu d'activer la ligne permettant de recevoir l'interruption de notre GPIO vers le MPU (33/32, reste 1 -> MIR1, BIT1).
- Ensuite on clear le masque avec le registre `MIR_CLEAR1` du MPU.
- Finalement, on peut activer la réception des interruptions sur le MPU avec le registre `INTC_CONTROL`.
- Désormais il faut activer les interruptions par le CPSR avec les commandes ASM donné dans le cours cours.
- Il faut ensuite définir la fonction `ìsr_handler()` qui sera appelé par le code assembleur (vu en cours). Cette fonction sera appelée à chaque interruption (vecteur d'interruption). Elle permettra d'acquitter l'interruption, de la traiter et pouvoir ensuite en accepter de nouvelles.

Pour tester cette partie, dans le `ìsr_handler()`, nous avons fait appelle à la fonciton `ToggleOutput` pour vérifier qu'à chaque appui du bouton SW0, la LED s'inversait.

## Etape 4 : Mise en oeuvre d’un timer

Le programme à réaliser a pour fonction de mesurer le temps écoulé entre l’allumage d’une led et l’appui sur un bouton à l’aide d’un timer.
- la Première des choses à faire est d'initialiser le timer en appelant la fonction `timer_init` dans le fichier `init.c` dans lequel les différents registres seront initialisés. Dans ce laboratoire, nous utiliserons `GPTIMER1`. 
- Avant de commencer à initialiser les registres, comme dans les précédentes sections, on effectue un reset software du timer.
- La seconde chose consiste à sélectionner l’horloge source du timer. nous devons configurer les registres `CM_CLKSEL` (permet de sélectionner le timer et sa fréquence), `CM_FCLKEN` (contrôle le module d'interface fonctionnel) et `CM_ICLKEN` (Activation de l'interface clock pour GPTIMER1) du module `PRCM`, plus précisément `WKUP_CM_REG`.
- A l'instar des précédentes sections, nous avons développé une toolbox qui permet de démarrer stopper, lire et écrire le timer. Nous avons utilisé 2 différents registre à savoir
    1. `TCLR` qui à le bit de control pour démarrer ou stopper le timer
    2. `TCRR` qui est utilisé pour lire ou écrire une valeur dans le timer

Les fonctions sont les suivantes: `start_timer`, `stop_timer`, `read_timer_value` et `write_timer_value`.

## Etape 5 : Application : Jeu mesure temps de réaction

Le jeu a été implémenté selon les spécifications de la consignes et de plus, certaines fonctionnalités supplémentaires ont été ajoutés comme l'enregistrement du meilleure score ou l'affichage du score des dernières parties.

## Conclusion

Ce laboratoire nous a permis d'appréhender la datasheet du DM3730 ainsi que le fonctionnement de la carte Reptar. En effet, il est de premier abord compliqué de naviguer dans les différentes sections du document et d'interpréter les actions nécessaires effectuer sur les registres. L'assistant nous à grandement aider dans cette démarche et dans les différentes parties du laboratoire. Toutes les parties on été implémentées, tesée et sont fonctionnelles.

# Laboratoire 3

## Introduction
L’objectif de ce laboratoire est d’écrire et tester les drivers et les fonctions de bas niveau qui permettent de communiquer avec une carte SD High Capacity. En particulier, on vous demande de réaliser les fonctions de base de lecture et d’écriture de données sur une carte SDHC.
Rappelons que pour ce laboratoire, comme pour les suivants du cours ASP, le programme écrit devra fonctionner en stand-alone sans utiliser de code déjà installé dans la carte, autrement dit : ni moniteur ni OS. Les drivers et fonctions réalisés seront utilisés dans le prochain labo.[tiré du laboratoire]

Pour rendre fonctionnel les « includes », il faut ajouter les fichiers init et toolbox au makefile initiale en générant les objet `.o`

Dans le cadre de ce laboratoire, ce sont les fichiers précédemment créer à l'attention du laboratoire 2 qui ont été utilisés.
Uniquement le fichier obj_i2c.o a été ajouté au makefile afin de le rendre fonctionnel l’interface.

Tous les tests ont été effectués avec le debugger, ainsi que la vérification des valeurs depuis celui-ci.

## Etape 1 : Initialisation de la carte SD
La solution a été fournie et a été testée en vérifiant que l'initialisation s'effectuait complétement mais surtout sans erreur.

## Etape 2 : Lire et afficher les informations de la carte SD

Dans cette étape, il s'agit d'accéder à la carte mémoire et de lire ses informations (nom + taille)
1. Afin de récupérer le registre CID, il faut envoyer la commande 10. De même pour le registre CSD, il faut envoyer la commande 9.
2. De sorte à de récupérer les valeurs des registres CSD et CID il est nécessaire de récupérer les réponses aux commandes avec les accès suivants :
- `MMCHS1_REG(MMCHS_RSP10)` permet d'accéder aux bits 31 à 0.
- `MMCHS1_REG(MMCHS_RSP32)` permet d'accéder aux bits 63 à 32.
- `MMCHS1_REG(MMCHS_RSP54)` permet d'accéder aux bits 95 à 64.
- `MMCHS1_REG(MMCHS_RSP76)` permet d'accéder aux bits 127 à 96.
3. De manière à récupérer le nom de produit, il est nécessaire de lire le registre CID aux bits [103:64] ensuite on l'enregistre dans un tableau de `char`. Ensuite en dernière position on ajoute un `\0` afin de marquer la terminaison de notre chaîne de caractère.
4. Pour récupérer la taille de la carte, à savoir `C_SIZE` dans le registre CSD, il faut accéder aux bits [73:62]. Ensuite pour connaître la capacité totale de la carte, nous effectuons cette transformation : ((C_SIZE + 1) * 512) Kbytes).


Série de tests effectués : 
 - Le nom a été affiché correctement 
 - La taille a été vérifiée depuis le mode debug de Code Composer
 

Avant de poursuivre dans les accès en écriture et lecture de la carte SD et afin de simplifier la compréhension des étapes qui vont suivre, il est nécessaire d'expliquer les registres et les flags des fonctionnalités qui ont été utilisés dans les étapes qui suivent:

- `MMCHS_PSTATE` : Registre permettant de connaître l'état actuel du contrôleur
- `MMCHS_IE` : Interrupt SD enable register, ce registre permet d'activer / désactiver le module pour définir les bits d'état, événement par événement
- `MCCHS_STAT` : Interrupt status register, le registre d'état d'interruption regroupe tous les états d'événements internes au module pouvant générer une interruption et leur état avec leur contrôle (flags).
- `MMCHS_SYSCTL` : SD system control register, registre de contrôle du système pour les cartes SD. Ce registre définit les commandes du système pour définir les réinitialisations logicielles, la fréquence d'horloge gestion et délai d'attente des données.

Dans un deuxième temps, il est nécessaire d'expliciter la position des bits des 
flags des différents registres : 

- `BRR` : Buffer Read Ready, permet de définir un transfert en lecture sans DMA.
- `BWR` : Buffer Write Ready, permet de définir un transfert en écriture sans DMA.
- `TC`  : Transfert complete, permet de savoir quand un transfert (lecture/écriture) est terminé.
- `DTO` : Data Timeout Error, cette valeur permet de savoir si l'intervalle selon lequel les délais d'attente des lignes mmci_dat est dépassé.
- `DEB` : Erreur de bit de données de fin, ce bit permet la détection d'un 0 à la position du bit de fin de lecture ou à la fin du statut CRC en mode écriture
- `DCRC`: Data CRC Error, ce bit permet de savoir s’il y aucune erreur CRC16 dans la phase de données
- `SRD` : Software reset for mmci_dat line,  ce bit est mis à 1 pour la réinitialisation et libérer RW 0
- `DATI` : Command inhibit, permet de savoir si des opérations de transferts sont en cours.
## Etape 3 : Lire un bloc de données
Le processus de lecture d'un unique bloc s'effectue de la manière suivante : 
1) Clear tous les statuts de MCCHS_STAT afin de quittancer les flags.
2) Activer la lecture en settant a 1 le bit du BRR et en settant a 0 le bit du BWR du registre MMCHS_IE
3) Effectuer une attente active sur MMCHS_PSTATE[1] (DATI) pour s'assurer que "data lines" n'est pas utilisé.
4) Envoyer la commande 17 a la carte SD avec la position du bloc pour qu'elle effectue la lecture d'un unique bloc.
5) Effectuer une attente active sur BRR de MMCHS_STAT jusqu'a ce qu'il nous retourne 1 et il faut le quittancer ceci pour nous assurer que le mode est bien BRR.
6) Les données sont disponibles et peuvent être lues. Afin de lire le bloc complet, il faut effectuer (BLEN+3/4) itérations. Dans notre cas un bloc est de 512kB.
7) Tant que le transfert n'est pas terminé (lecture du bit TC du registre MMCHS_STAT), nous contrôlons que le transfert des données n'occasionne aucune erreur.
8) Le contrôle s'effectue sur DTO, DEB et DCRC du registre MMCHS_STAT. S'il y a une erreur on set à 1 SRD du registre MMCHS_SYSCTL et on boucle dessus jusqu'à ce qu'il nous retourne un 1. Ceci termine sur un échec de la lecture d'un bloc sur la carte SD.
9) S'il n'y a pas eu d'erreur, et que le bit TC de MMCHS_STAT est à 1, le transfert s'est accomplis sans erreur. Afin de terminer la lecture, on quittance tous les flags de MMCHS_STAT.

Cette étape a été testée de la manière suivante :
 - Plusieurs lecture du même bloc.
 - Plusieurs lecture avec des blocs différents.


## Etape 4 : Lire plusieurs blocs de données
Le processus d'écriture d'un unique bloc s'effectue de manière très similaire à la lecture d'un bloc, de ce fait, ne seront expliqué que les étapes du processus d'écriture qui diffère de celui de lecture, à savoir les étapes suivantes :

2) Activer la lecture en settant a 1 le bit du BWR et en settant a 0 le bit du BRR du registre MMCHS_IE.
4) Envoyer la commande 24 a la carte SD avec la position du bloc pour qu'elle effectue l'écriture d'un unique bloc.
5) Effectuer une attente active sur BWR de MMCHS_STAT jusqu'à ce qu'il nous retourne 1 et il faut le quittancer ceci pour nous assurer que le mode est bien BWR.
6) Les données peuvent être écrite. Afin de d'écrire le bloc complet, il faut effectuer (BLEN+3/4) itérations. Dans notre cas un bloc est de 512kB

L'étape 4 a été testé de la manière suivante :
 - Plusieurs écritures sur le même bloc.
 - Plusieurs écritures sur des blocs à différents endroits.


## Etape 5 : Écrire un bloc de données
 A l'instar de la lecture, d'un seul bloc, la lecture multiple est très proche de la lecture d'un bloc. De la même manière que l'écriture d'un bloc, uniquement les étapes différentes seront décrites avec le numéro de l'opération.

0) Une étape supplémentaire initiale est ajouté. Elle permet de vérifier que le nombre de blocs demandé en lecture soit supérieur à 0. En effet, si le nombre de bloc en lecture est de 0, il est sensé retourné qu'aucun bloc n'a pu être de lu. De plus, si un seul bloc est demandé en lecture, nous allons utiliser la fonction précédemment explicitée pour effectuer l'opération d'un unique bloc. 
4) Envoyer la commande 18 a la carte SD avec la position du bloc et le nombre de bloc à lire pour qu'elle effectue la lecture multiple.
6) Les données sont disponibles et peuvent être lues. Afin de lire les blocs complets, il faut effectuer (BLEN+3/4) * le nombre de bloc voulu d'itérations.
10) La lecture de plusieurs blocs demande une seconde étape supplémentaire. En effet, nous devons envoyer la commande 12 pour ne pas stopper la lecture consécutive de bloc.

L'étape 5 a été testé de la manière suivante :
 - Lecture de 0 bloc et vérification du contrôle.
 - Lecture de 1 bloc et vérification du contrôle et de la sortie.
 - Lecture de 2 blocs et vérification de la sortie.
 - Lecture de 8 blocs et vérification de la sortie.
 - Lecture de n>8 blocs et vérification du contrôle.

## Etape 6 : Écrire plusieurs blocs de données
L'étape 6 sera traité de la même manière que l'étape 5.
4) Envoyer la commande 25 a la carte SD avec la position du bloc et le nombre de bloc à écrire pour qu'elle effectue l'écriture multiple.
6) Les données peuvent être écrites. Afin d'écrire tous les blocs complets, il faut effectuer (BLEN+3/4) * le nombre de bloc désiré d'itérations.

L'étape 6 a été testé de la manière suivante :
 - Ecriture de 0 bloc et vérification du contrôle.
 - Ecriture de 1 bloc et vérification du contrôle et de la sortie.
 - Ecriture de 2 blocs et vérification de la sortie.
 - Ecriture de 8 blocs et vérification de la sortie.
 - Ecriture de n>8 blocs et vérification du contrôle.

Conclusion :
Ce laboratoire nous a permis de nous confronter à une activité concrète que nous utilisons presque tous les jours sans nous rendre compte. Je l'ai trouvé très intéressant.
Le contenu entier du laboratoire a été implémenté et tous les tests sont passés. De plus, la solution a été vérifiée par l'assistant. Ce laboratoire était beaucoup plus dirigé que le précédent et je le trouvais plus accessible.
