/*
 * timer_toolbox.h
 *
 *  Created on: Apr 6, 2019
 *      Author: redsuser
 */

#ifndef TIMER_TOOLBOX_H_
#define TIMER_TOOLBOX_H_

#include "stddefs.h"
#include "bits.h"
#include "timer.h"

extern void start_timer(uchar module);
extern void stop_timer(uchar module);
extern ulong read_timer_value(uchar module);
extern void write_timer_value(uchar module, ulong value);

#endif /* TIMER_TOOLBOX_H_ */
