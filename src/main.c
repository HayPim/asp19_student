/*---------------------------------------------------------------------------------------
 * But               : Petit programme pour etre charge dans le micro et affiche avec dump
 * Auteur            : Evangelina LOLIVIER-EXLER
 * Date              : 29.07.2008
 * Version           : 1.0
 * Fichier           : demo.c
 *----------------------------------------------------------------------------------------*/
#include "cfg.h"
#include "stddefs.h"
#include "init.h"
#include "lcd_toolbox.h"
#include "gpio_toolbox.h"
#include "gpio.h"
#include "bits.h"
#include "timer_toolbox.h"
#include "reflexTester.h"



/* Global variables */
vulong AppStack_svr[APPSTACKSIZE/4];
vulong AppStack_irq[APPSTACKSIZE/4];
int t[8];


int begin = 1;

// Blocks de donnes qui vont etre utilises pour la lecture et ecriture
ulong ul_1block_read[128];
ulong ul_1block_write[128];


uchar uc_1block_read[128 * 4 * 1];
uchar uc_1block_write[128 * 4 * 1];

uchar uc_2blocks_read[128 * 4 * 2];
uchar uc_2blocks_write[128 * 4 * 2];

uchar uc_8blocks_read[128 * 4 * 8];
uchar uc_8blocks_write[128 * 4 * 8];

int error;

// Variable permettant de recuperer le nom et la taille de la carte memoire
uchar sdName[6];
ulong cardSize;



/* main */

void general_init(){
	lcd_off();
	lcd_init();
	lcd_on();
	clear_screen();
	GPIO_init();
	//interrupt_init();
	//timer_init();
	//game_initialize();
	mmc1_init();

}

int main(void)
{



    int i;
    for (i=0;i<8;i++) {
        t[i] = i*(i-4);
    }

    asm("MOV r0, #7\n");

    if(begin){
    	general_init();
    	begin = 0;
    	error = 0;

		sdhc_init();
		read_productname(sdName);
		fb_print_string(sdName, 60, 50, 3);
		cardSize = read_card_size();


		// Boucle faite avec l'assistant pour valider le fonctionnement du laboratroire
	    int j;
	    for (j = 0; j < 512; ++j) {
	    	uc_1block_write[j] = 0xAA;
	    	uc_1block_read[j] = 0x00;
	    }

	    for (j = 0; j < 512 * 8 ; ++j) {
			uc_8blocks_write[j] = 0xFF;
			uc_8blocks_read[j] = 0x00;
		}

		mmchs_write_block(uc_1block_write, 0);
		mmchs_write_block(uc_1block_write, 4);
		mmchs_read_block(uc_1block_read, 0);
		mmchs_read_block(uc_1block_read, 4);

		mmchs_write_multiple_block(uc_8blocks_write, 0, 3);
		mmchs_read_multiple_block(uc_8blocks_read, 0, 3);



		/*
		 * Ceci sont des tests pour valider l'ecriture et la lecture
		// Test avec 1 bloc
		error = mmchs_write_block(ul_1block_write, 7);
		error = mmchs_read_block(ul_1block_read, 7);


		// Test avec 0 blocs dans une ecriture multiple
		error = mmchs_write_multiple_block(uc_1block_write, 8, 0);
		error = mmchs_read_multiple_block(uc_1block_read, 8, 0);

		// Test avec 1 blocs dans une ecriture multiple
		error = mmchs_write_multiple_block(uc_1block_write, 8, 1);
		error = mmchs_read_multiple_block(uc_1block_read, 8, 1);

		// Test avec 2 blocs
		error = mmchs_write_multiple_block(uc_2blocks_write, 8, 2);
		error = mmchs_read_multiple_block(uc_2blocks_read, 8, 2);

		// Test avec 8 bloc
		error = mmchs_write_multiple_block(uc_2blocks_write, 8, 8);
		error = mmchs_read_multiple_block(uc_2blocks_read, 8, 8);

		// Test avec plus de 8 bloc
		error = mmchs_write_multiple_block(uc_2blocks_write, 8, 9);
		error = mmchs_read_multiple_block(uc_2blocks_read, 8, 9);
		*/
    }

    while (1);

    return(0);
}
