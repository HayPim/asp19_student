/*
 * reflexTester.c
 *
 *  Created on: Apr 6, 2019
 *      Author: redsuser
 */

#include "reflexTester.h"

#define TEMPS_MIN 	1000000
#define TEMPS_MAX 	1000000000
#define COLORLCD	3
#define MAXNBRGAME	5
#define MAXLENGTHPRINT	12
#define BADSCORE  	-1


ulong score = 0;

void new_game(){
	int nbrGame = 0;
	ulong bestScore = BADSCORE;
	char intToStringScore[MAXLENGTHPRINT];
	ulong saveScore[MAXNBRGAME];
	while(1){
		clear_screen();
		game_initialize();
		int i = 0;

		if(bestScore != BADSCORE){
			sprintf(intToStringScore, "%lo", bestScore);
			fb_print_string((uchar*)"Best score : ", 60, 75, COLORLCD);
			fb_print_string((uchar*)intToStringScore, 60 + 10 * MAXLENGTHPRINT, 75, COLORLCD);
			fb_print_string((uchar*)"[ms]", 200 + 10 * MAXLENGTHPRINT, 75, COLORLCD);
		}



		// Permet d'afficher le score si une partie a deja� ete joue
		for(i = 0; i < nbrGame; ++i) {
			sprintf(intToStringScore, "%lo", saveScore[i]);
			fb_print_string((uchar*)"Votre resultat : ", 60, 90 + 10*i, COLORLCD);
			fb_print_string((uchar*)intToStringScore, 200, 90 + 10*i, COLORLCD);
			fb_print_string((uchar*)"[ms]", 200 + 10 * MAXLENGTHPRINT, 90 + 10*i, COLORLCD);
		}

		fb_print_string((uchar*)"**Test des reflexes**", 60, 40, COLORLCD);
		fb_print_string((uchar*)"Pour une nouvelle partie, appuye sur 1", 60, 50, COLORLCD);
	
		// On attends que le bouton soit lach� avant de pouvoir recommencer une nouvelle partie
		while(ReadInput(GPIO5, SW2));
		while(!ReadInput(GPIO5, SW2));

		// On efface l'écran avant d'afficher le le lancement du jeux avant d'allumer la led et de démarrer le timer
		clear_screen();
		fb_print_string((uchar*)"START!", 60, 40, COLORLCD);
		udelay(rand() % (TEMPS_MAX - TEMPS_MIN + 1) + 1);
		SetOutput(GPIO5, LED0);
		start_timer(TIMER1);
		while(!ReadInput(GPIO5, SW1));


		if(score < bestScore){
			bestScore = score;
		}

		sprintf(intToStringScore, "%lo", score);
		saveScore[nbrGame++] = score;

		if(nbrGame == MAXNBRGAME){
			nbrGame = 0;
		}
		write_timer_value(TIMER1,0);
	}
}

// Fonction appelé lors d'une interruption
// Permet de d'arreter le timer et de sauvegarder le score
void getScore(){
    stop_timer(TIMER1);
    score = read_timer_value(TIMER1) / CLKFREQENCYKHZ;
}

void game_initialize(){
	// Permet de remettre l'état se toutes les leds à 0
	ClearOutput(GPIO5, LED0);
	ClearOutput(GPIO5, LED1);
}
