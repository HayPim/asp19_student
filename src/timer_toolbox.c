/*
 * timer_toolbox.c
 *
 *  Created on: Apr 6, 2019
 *      Author: redsuser
 */

#include "timer_toolbox.h"


void start_timer(uchar module){
	switch(module){
	case 1:
		GPT1_REG(TCLR) |= BIT0;
		break;
	case 2:
		GPT2_REG(TCLR) |= BIT0;
		break;
	case 3:
		GPT3_REG(TCLR) |= BIT0;
		break;
	case 4:
		GPT4_REG(TCLR) |= BIT0;
		break;
	case 5:
		GPT5_REG(TCLR) |= BIT0;
		break;
	case 6:
		GPT6_REG(TCLR) |= BIT0;
		break;
	case 7:
		GPT7_REG(TCLR) |= BIT0;
		break;
	case 8:
		GPT8_REG(TCLR) |= BIT0;
		break;
	case 9:
		GPT9_REG(TCLR) |= BIT0;
		break;
	case 10:
		GPT10_REG(TCLR) |= BIT0;
		break;
	case 11:
		GPT11_REG(TCLR) |= BIT0;
		break;
	}
}
void stop_timer(uchar module){
	switch(module){
	case 1:
		GPT1_REG(TCLR) &= ~BIT0;
		break;
	case 2:
		GPT2_REG(TCLR) &= ~BIT0;
		break;
	case 3:
		GPT3_REG(TCLR) &= ~BIT0;
		break;
	case 4:
		GPT4_REG(TCLR) &= ~BIT0;
		break;
	case 5:
		GPT5_REG(TCLR) &= ~BIT0;
		break;
	case 6:
		GPT6_REG(TCLR) &= ~BIT0;
		break;
	case 7:
		GPT7_REG(TCLR) &= ~BIT0;
		break;
	case 8:
		GPT8_REG(TCLR) &= ~BIT0;
		break;
	case 9:
		GPT9_REG(TCLR) &= ~BIT0;
		break;
	case 10:
		GPT10_REG(TCLR) &= ~BIT0;
		break;
	case 11:
		GPT11_REG(TCLR) &= ~BIT0;
		break;
	}
}
ulong read_timer_value(uchar module){
	switch(module){
	case 1:
		return GPT1_REG(TCRR);
	case 2:
		return GPT2_REG(TCRR);
	case 3:
		return GPT3_REG(TCRR);
	case 4:
		return GPT4_REG(TCRR);
	case 5:
		return GPT5_REG(TCRR);
	case 6:
		return GPT6_REG(TCRR);
	case 7:
		return GPT7_REG(TCRR);
	case 8:
		return GPT8_REG(TCRR);
	case 9:
		return GPT9_REG(TCRR);
	case 10:
		return GPT10_REG(TCRR);
	case 11:
		return GPT11_REG(TCRR);
	}
}
void write_timer_value(uchar module, ulong value){
	switch(module){
	case 1:
		GPT1_REG(TCRR) = value;
		break;
	case 2:
		GPT2_REG(TCRR) = value;
		break;
	case 3:
		GPT3_REG(TCRR) = value;
		break;
	case 4:
		GPT4_REG(TCRR) = value;
		break;
	case 5:
		GPT5_REG(TCRR) = value;
		break;
	case 6:
		GPT6_REG(TCRR) = value;
		break;
	case 7:
		GPT7_REG(TCRR) = value;
		break;
	case 8:
		GPT8_REG(TCRR) = value;
		break;
	case 9:
		GPT9_REG(TCRR) = value;
		break;
	case 10:
		GPT10_REG(TCRR) = value;
		break;
	case 11:
		GPT11_REG(TCRR) = value;
		break;
	}
}
