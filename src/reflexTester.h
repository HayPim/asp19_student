/*
 * reflexTester.h
 *
 *  Created on: Apr 6, 2019
 *      Author: redsuser
 */

#ifndef REFLEXTESTER_H_
#define REFLEXTESTER_H_

#include "cfg.h"
#include "stddefs.h"
#include "init.h"
#include "lcd_toolbox.h"
#include "gpio_toolbox.h"
#include "gpio.h"
#include "bits.h"
#include "timer.h"
#include "timer_toolbox.h"

void new_game();
void getScore();
void game_initialize();

#endif /* REFLEXTESTER_H_ */
