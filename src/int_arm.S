/*
 * int_arm.S
 *
 *  Created on: Mar 28, 2019
 *      Author: redsuser
 */

.extern isr_handler
.global isr
 
isr:

	stmfd sp!, {r0-r12, lr}
	bl isr_handler
	ldmfd sp!, {r0-r12, lr}
	subs pc, lr, #4	
