/*
 * gpio_toolbox.c
 *
 *  Created on: Mar 19, 2019
 *      Author: redsuser
 */

#include "gpio_toolbox.h"
#include "gpio.h"

void SetOutput(uchar module, ulong bitmask){
	switch(module){
	case 1:
		GPIO1_REG(OMAP_GPIO_SETDATAOUT) = bitmask;
		break;
	case 2:
		GPIO2_REG(OMAP_GPIO_SETDATAOUT) = bitmask;
		break;
	case 3:
		GPIO3_REG(OMAP_GPIO_SETDATAOUT) = bitmask;
		break;
	case 4:
		GPIO4_REG(OMAP_GPIO_SETDATAOUT) = bitmask;
		break;
	case 5:
		GPIO5_REG(OMAP_GPIO_SETDATAOUT) = bitmask;
		break;
	case 6:
		GPIO6_REG(OMAP_GPIO_SETDATAOUT) = bitmask;
		break;
	}
}
void ClearOutput(uchar module, ulong bitmask){
	switch(module){
	case 1:
		GPIO1_REG(OMAP_GPIO_CLEARDATAOUT) = bitmask;
		break;
	case 2:
		GPIO2_REG(OMAP_GPIO_CLEARDATAOUT) = bitmask;
		break;
	case 3:
		GPIO3_REG(OMAP_GPIO_CLEARDATAOUT) = bitmask;
		break;
	case 4:
		GPIO4_REG(OMAP_GPIO_CLEARDATAOUT) = bitmask;
		break;
	case 5:
		GPIO5_REG(OMAP_GPIO_CLEARDATAOUT) = bitmask;
		break;
	case 6:
		GPIO5_REG(OMAP_GPIO_CLEARDATAOUT) = bitmask;
		break;
	}
}
uchar ReadInput(uchar module, ulong bitmask){
	ulong data;
	switch(module){
	case 1:
		data = GPIO1_REG(OMAP_GPIO_DATAIN) & bitmask;
		break;
	case 2:
		data = GPIO2_REG(OMAP_GPIO_DATAIN) & bitmask;
		break;
	case 3:
		data = GPIO3_REG(OMAP_GPIO_DATAIN) & bitmask;
		break;
	case 4:
		data = GPIO4_REG(OMAP_GPIO_DATAIN) & bitmask;
		break;
	case 5:
		data = GPIO5_REG(OMAP_GPIO_DATAIN) & bitmask;
		break;
	case 6:
		data = GPIO6_REG(OMAP_GPIO_DATAIN) & bitmask;
		break;
	}
	return data ? 1 : 0;
}
void ToggleOutput(uchar module, ulong bitmask){
	switch(module){
	case 1:
		GPIO1_REG(OMAP_GPIO_DATAOUT) ^= bitmask;
		break;
	case 2:
		GPIO2_REG(OMAP_GPIO_DATAOUT) ^= bitmask;
		break;
	case 3:
		GPIO3_REG(OMAP_GPIO_DATAOUT) ^= bitmask;
		break;
	case 4:
		GPIO4_REG(OMAP_GPIO_DATAOUT) ^= bitmask;
		break;
	case 5:
		GPIO5_REG(OMAP_GPIO_DATAOUT) ^= bitmask;
		break;
	case 6:
		GPIO6_REG(OMAP_GPIO_DATAOUT) ^= bitmask;
		break;
	}
}
void UnmaskIRQ(uchar module, ulong bitmask){

}
void MaskIRQ(uchar module, ulong bitmask){

}
